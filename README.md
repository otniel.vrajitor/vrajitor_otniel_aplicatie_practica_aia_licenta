# Vrajitor_Otniel_Aplicatie_Practica_AIA_Licenta

[AUTOMATIZAREA SISTEMULUI DE MONITORIZARE AL PLĂŢII PARCĂRILOR](https://gitlab.upt.ro/otniel.vrajitor/vrajitor_otniel_aplicatie_practica_aia_licenta.git)  

adresa gitlab: https://gitlab.upt.ro/otniel.vrajitor/vrajitor_otniel_aplicatie_practica_aia_licenta.git
#### Candidat: Vrajitor Otniel
#### Coordonator: prof.dr.ing. Lăcrămioara Stoicu-Tivadar
## Instalare

Mediul de dezvolatere Pycharm se poate instala de la urmatoarea [adreasa](https://www.jetbrains.com/pycharm/download/#section=windows).  
Dupa ce ati deschis fisierul proiectului in Pycharm, cu ajutorul terminalului integrat instalati Pytorch urmand instructiunile de [aici](https://pytorch.org/get-started/locally/).  
Urmatorul pas este instalarea bibliotecilor folosite. Pentru a face acest lucru am creat un fisier text cu toate pachetele care trebuie instalate. Pentru a instala pachetele mergeti in terminalul din Pycharm si rulati comanda "pip install -r requirements.txt".  
## Lansarea aplicatiei

Pentru lansarea aplicatiei puteti folosi fie comanda "python server.py" in terminal fie click dreapta pe fisierul "server.py" si apasati "run".  
###### Pentru dezvoltarea aplicatiei s-a folosit python versiunea 3.9.13 
