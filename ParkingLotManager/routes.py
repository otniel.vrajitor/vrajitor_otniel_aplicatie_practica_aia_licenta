from flask import render_template, redirect, url_for, flash, Response
from flask_login import login_user, logout_user, current_user

from ParkingLotManager import app, db, bcrypt
from ParkingLotManager.forms import LoginForm, RegistrationForm, LicensePlateForm
from ParkingLotManager.models import User, LicensePlate
from ParkingLotManager.camera import get_latest_frame, perform_ocr

from datetime import datetime, timedelta


@app.route("/")
def home():
    if current_user.is_authenticated:
        if current_user.admin:
            return redirect(url_for("admin"))
        else:
            return redirect(url_for("user"))

    else:
        return redirect(url_for("login"))


@app.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("home"))

    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode(
            "utf-8"
        )

        user = User(
            email=form.email.data,
            password=hashed_password,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            address=form.address.data,
        )

        db.session.add(user)
        db.session.commit()

        flash("Account created successfully", "success")
        return redirect(url_for("login"))

    return render_template("register.html", form=form)


@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("home"))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=True)
            return redirect(url_for("home"))
        else:
            flash("Email or password is incorrect", "danger")

    return render_template("login.html", form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("login"))


@app.route("/user", methods=["GET", "POST"])
def user():
    if current_user.is_authenticated:
        if current_user.admin:
            return redirect(url_for("home"))
    else:
        return redirect(url_for("home"))

    form = LicensePlateForm()
    if form.validate_on_submit():
        license_plate = LicensePlate(
            value=form.license_plate.data,
            expiry=datetime.utcnow() + timedelta(hours=int(form.parking_duration.data)),
            user_id=current_user.id,
        )

        db.session.add(license_plate)
        db.session.commit()

        flash(
            f"License plate has been successfully registered for {int(form.parking_duration.data)} hour(s)",
            "success",
        )

        return redirect(url_for("user"))

    registered_plates = LicensePlate.query.filter_by(user_id=current_user.id)

    return render_template("user.html", form=form, registered_plates=registered_plates)


@app.route("/admin")
def admin():
    if current_user.is_authenticated:
        if not current_user.admin:
            return redirect(url_for("home"))
    else:
        return redirect(url_for("home"))

    return render_template("admin.html")


@app.route("/plates")
def plates():
    detected_plates = perform_ocr()
    print(detected_plates)

    not_registered_plates = []
    for plate in detected_plates:
        if not LicensePlate.query.filter_by(value=plate).first():
            not_registered_plates.append(plate)

    return not_registered_plates


@app.route("/video")
def video():
    return Response(
        get_latest_frame(), mimetype="multipart/x-mixed-replace; boundary=frame"
    )
