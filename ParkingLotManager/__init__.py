from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager

from datetime import datetime
import atexit
from apscheduler.schedulers.background import BackgroundScheduler
from threading import Thread

app = Flask(__name__)
app.config[
    "SECRET_KEY"
] = "8397bc750f3edc0d3b06a401b798c9f1b79d10f2e01a54960ed273b14418b5d3"
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///parking_lot.db"
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)


from ParkingLotManager import routes
from ParkingLotManager.models import User, LicensePlate
from ParkingLotManager.camera import read_frames


def remove_expired_plates():
    with app.app_context():
        expired_plates = LicensePlate.query.filter(
            LicensePlate.expiry <= datetime.utcnow()
        )
        for plate in expired_plates:
            db.session.delete(plate)
        db.session.commit()


scheduler = BackgroundScheduler()
scheduler.add_job(func=remove_expired_plates, trigger="interval", seconds=60)
scheduler.start()
atexit.register(lambda: scheduler.shutdown())

with app.app_context():
    Thread(target=read_frames, daemon=True).start()

    db.create_all()

    if User.query.filter_by(email="admin@gmail.com").first() is None:
        password = "admin123"
        hashed_password = bcrypt.generate_password_hash(password).decode("utf-8")

        user = User(
            email="admin@gmail.com",
            password=hashed_password,
            first_name="Admin",
            last_name="Admin",
            address="Admin",
            admin=True,
        )

        db.session.add(user)
        db.session.commit()
