from flask_login import UserMixin

from ParkingLotManager import db, login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(60), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    address = db.Column(db.String(200), nullable=False)
    license_plates = db.relationship("LicensePlate", backref="user", lazy=True)

    admin = db.Column(db.Boolean, default=False, nullable=False)


class LicensePlate(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(7), unique=True, nullable=False)
    expiry = db.Column(db.DateTime, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
