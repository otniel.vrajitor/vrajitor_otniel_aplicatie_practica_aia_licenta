from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, TextAreaField, RadioField
from wtforms.validators import (
    InputRequired,
    Email,
    Length,
    EqualTo,
    Regexp,
    ValidationError,
)

from ParkingLotManager.models import User, LicensePlate


class LoginForm(FlaskForm):
    email = StringField(
        "Email",
        validators=[InputRequired(), Email("Please provide a valid email address.")],
    )
    password = PasswordField("Password", validators=[InputRequired()])
    submit = SubmitField("Login")


class RegistrationForm(FlaskForm):
    email = StringField(
        "Email",
        validators=[InputRequired(), Email("Please provide a valid email address.")],
    )
    password = PasswordField("Password", validators=[InputRequired(), Length(min=8)])
    confirm_password = PasswordField(
        "Confirm Password", validators=[InputRequired(), EqualTo("password")]
    )
    first_name = StringField("First Name", validators=[InputRequired(), Length(max=50)])
    last_name = StringField("Last Name", validators=[InputRequired(), Length(max=50)])

    address = TextAreaField("Address", validators=[InputRequired(), Length(max=200)])
    submit = SubmitField("Register")

    def validate_email(self, email):
        if User.query.filter_by(email=email.data).first():
            raise ValidationError("Account with that email already exists.")


class LicensePlateForm(FlaskForm):
    license_plate = StringField(
        "License Plate",
        validators=[
            InputRequired(),
            Length(max=7),
            Regexp(
                r"^[A-Z]{2}\d{2}[A-Z]{3}$",
                message="Please input a valid license plate. There must be no spaces and all characters must be uppercase.",
            ),
        ],
    )

    parking_duration = RadioField(
        "Parking Duration",
        choices=[("1", "1 Hour"), ("24", "24 Hours")],
        validators=[InputRequired()],
    )

    submit = SubmitField("Submit")

    def validate_license_plate(self, license_plate):
        if LicensePlate.query.filter_by(value=license_plate.data).first():
            raise ValidationError(
                "That license plate is already registered for parking."
            )
