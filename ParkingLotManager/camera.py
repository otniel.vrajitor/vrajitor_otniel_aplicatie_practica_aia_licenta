import cv2
import easyocr
import re
import time
from pathlib import Path

reader = easyocr.Reader(["en"], gpu=True)
license_plate_pattern = r"[A-Z]{2}\d{2}[A-Z]{3}"
special_characters = r'[!"#$%&\'()*+,-./:;<=>?@\[\\\]^_`{\|}~\s]'

current_frame = None


def read_frames():
    global current_frame

    # Video mode
    while True:
        video_path = str(
            Path(__file__).parent.absolute().joinpath("static/20230513_165447.mp4")
        )
        cap = cv2.VideoCapture(video_path)
        fps = cap.get(cv2.CAP_PROP_FPS)
        while True:
            now = time.time()
            ret, frame = cap.read()
            if not ret:
                break

            current_frame = frame

            time_elapsed = time.time() - now
            if time_elapsed < (1 / fps):
                time.sleep((1 / fps) - time_elapsed)

    # # Camera mode
    # cap = cv2.VideoCapture(0)
    # while True:
    #     ret, frame = cap.read()
    #     if not ret:
    #         break

    #     current_frame = frame


def perform_ocr():
    gray = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)
    blurred = cv2.bilateralFilter(gray, 11, 17, 17)
    _, thresh = cv2.threshold(
        blurred, 100, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU
    )
    results = reader.readtext(thresh)

    detected_plates = []
    for bbox, text, prob in results:
        (tl, tr, br, bl) = bbox

        tl = (int(tl[0]), int(tl[1]))
        br = (int(br[0]), int(br[1]))

        aspect_ratio = (br[0] - tl[0]) / (br[1] - tl[1])

        if aspect_ratio > 2 and aspect_ratio < 5 and prob > 0.5:
            text = re.sub(special_characters, "", text)
            match = re.search(license_plate_pattern, text)
            if match:
                detected_plates.append(match.group())

    return detected_plates


def get_latest_frame():
    while True:
        if current_frame is not None:
            ret, buffer = cv2.imencode(".jpeg", current_frame)
            if not ret:
                break
            frame = buffer.tobytes()
            yield (b"--frame\r\n" b"Content-type: image/jpeg\r\n\r\n" + frame + b"\r\n")
